# Honeycrisp

Honeycrisp is a utility app for Spotify. Initially, it will be for creating
smart playlists, similar to what you can do in iTunes and Apple Music.
Eventually, I'll probably add more functionality.

## Authentication

Currently the authentication is all client-side, using Spotify's
[implicit grant flow authorization scheme](https://developer.spotify.com/documentation/general/guides/authorization-guide/#implicit-grant-flow).
This means that the granted API credentials don't automatically refresh, 
and expire after 1 hour. 

That could be a bit annoying in some cases, but for now it's better than
having to implement a backend. In the future, a backend authentication service
may be implemented via AWS lambdas to manage user sessions. For now, I just want
to get the app up and running.