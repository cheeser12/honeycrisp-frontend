import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <mat-toolbar color="primary" class="navbar">
      <nav>
        <span>{{ title }}</span>
      </nav>
    </mat-toolbar>
  `,
  styleUrls: ['app-navbar.component.scss']
})
export class AppNavbarComponent {
  @Input()
  public title: string;
}
