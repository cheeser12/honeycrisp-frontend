import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { AppLoginComponent } from '../app-login.component';

@Component({
  selector: 'app-home',
  template: `
    <h1>Hello Home</h1>
  `
})
export class AppHomeComponent implements OnInit {
  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const params = new URLSearchParams(this.route.snapshot.fragment);
    const accessToken = params.get('access_token');

    if (accessToken) {
      sessionStorage.setItem('access_token', accessToken);

      const expiryDate = new Date();
      const expiresIn = parseInt(params.get('expires_in'));
      expiryDate.setSeconds(expiryDate.getSeconds() + expiresIn);

      sessionStorage.setItem('access_token_expires', expiryDate.toString());

      window.history.replaceState(null, null, window.location.origin);
    } else if (sessionStorage.getItem('access_token')) {
      this.checkExpiredToken();
    } else {
      const dialogRef = this.dialog.open(AppLoginComponent, {
        disableClose: true
      });
    }
  }

  private checkExpiredToken() {
    const accessTokenExpiry = new Date(sessionStorage.getItem('access_token_expires'));

    if (new Date() >= accessTokenExpiry) {
      sessionStorage.removeItem('access_token');
      sessionStorage.removeItem('access_token_expires');
    }
  }
}
