import { Component } from '@angular/core';
import * as SpotifyWebApi from 'spotify-web-api-js';

@Component({
  selector: 'app-login',
  template: `
    <h1 mat-dialog-title>Login</h1>
    <div mat-dialog-content>
      In order to use Honeycrisp, you need to login with Spotify.
      <br>
      <br>
      <button (click)="onLoginClick()" color="accent" mat-flat-button>
        <img class="spotifyIcon" src="../assets/img/Spotify_Icon_RGB_Black.png">&nbsp;&nbsp;Login with Spotify
      </button>
    </div>
  `,
  styleUrls: ['app-login.component.scss']
})
export class AppLoginComponent {
  onLoginClick() {
    window.location.href = 'https://accounts.spotify.com/authorize?client_id=71c3609c62434157abaf9e00831ce009&response_type=token&redirect_uri=http://localhost:4200';
  }
}
